/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}

var nadimki = [];
function nastaviNadimek(uporabnik, nadimek) {
  nadimki[uporabnik] = nadimek;
}
function dobiNadimek(uporabnik) {
  return nadimki[uporabnik];
}
function dobiNadimekString(uporabnik) {
  var nadimek = dobiNadimek(uporabnik);
  if (nadimek == undefined)
    return uporabnik;
  return nadimek + " (" + uporabnik + ")";
}
function remapNadimek(sporocilo) {
  var arr = sporocilo.split(" se je preimenoval v ");
  if (arr.length == 2) {
    if (nadimki[arr[0]] != undefined) {
      arr[1] = arr[1].substring(0, arr[1].length - 1);
      nadimki[arr[1]] = nadimki[arr[0]];
      nadimki[arr[0]] = undefined;
    }
  }
}

/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  var jeKrc = sporocilo.indexOf("&#9756;") > -1;
  if (jeSmesko || jeKrc) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 *  Prikaz slik iz sporocila
 */
function poslikaj(sporocilo) {
  // Preveri vse besede ce so slike
  besede = sporocilo.split(" ");
  if (besede[0] == "/zasebno" && besede.length >= 3){
    besede.shift();
    besede.shift();
    besede = besede.join(" ");
    if (besede.charAt(0) == "\"" && besede.charAt(besede.length-1) == "\"") {
      besede = besede.substring(1, besede.length - 1);
      besede = besede.split(" ");
    }
  }
  
  for (var i = 0; i < besede.length; i++) {
    if (besede[i].startsWith("http")) {
      if (
          besede[i].endsWith(".jpg") ||
          besede[i].endsWith(".png") ||
          besede[i].endsWith(".gif")
         ) {
           
        $('#sporocila').append("<img style=\"margin-left:20px;width:200px\" src=\"" + besede[i] + "\"/><br/>");
      }
    }
  }
  
  // Border case (zadnja beseda koncana z locilom)
  beseda = besede[besede.length - 1];
  beseda = beseda.substring(0, beseda.length - 1);
  if (beseda.startsWith("http")) {
    if (
        beseda.endsWith(".jpg") ||
        beseda.endsWith(".png") ||
        beseda.endsWith(".gif")
       ) {
         
      $('#sporocila').append("<img style=\"margin-left:20px;width:200px\" src=\"" + beseda + "\"/><br/>");
    }
  }
  
  console.log("end");
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }
  poslikaj(sporocilo);
  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    remapNadimek(sporocilo.besedilo);
    var msg = sporocilo.besedilo.split(": ");
    if (msg.length > 0)
      msg[0] = dobiNadimekString(msg[0]);
    msg = msg.join(": ");
    var novElement = divElementEnostavniTekst(msg);
    $('#sporocila').append(novElement);
    poslikaj(sporocilo.besedilo);
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    for (var i=0; i < uporabniki.length; i++) {
      var u = divElementEnostavniTekst(dobiNadimekString(uporabniki[i]));
      $('#seznam-uporabnikov').append(
        "<a onClick=\"krc('" +
          uporabniki[i].split("\"").join("&#34;").split("'").join("&#39;") +
        "')\">" +
          u.text() +
        "</a><br/>"
      );
    }
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});

function krc(uporabnik) {
    uporabnik.split("&#34;").join("'").split("&#39;").join("\"");
    this.socket.emit('sporocilo', {vzdevek: uporabnik, besedilo: "&#9756;"});
  }
